var express = require('express');
var router = express.Router();

const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();


// controller
const {MessageController} = require('../controllers')

router.get(
    '/:address', jsonParser,
    async function(req, res) {
      await MessageController.getMessages(req, res)
    });

module.exports = router;
