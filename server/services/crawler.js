'use strict'
const CronJob = require('cron').CronJob;

//Model
const Model = require('../models')

//Util
const { getLatestBlockNumber, formatAddress } = require('../utils/eth')

const BillboardContract = require('../config/blockchain/billboard')

let config

async function _calculateProcessBlock() {
    const latestNetworkBlock = await getLatestBlockNumber()
    const safeProcessBock = latestNetworkBlock - config.confirmed
    let fromBlockNumber = config.blockProcessed + 1
    if (fromBlockNumber > safeProcessBock) {
        return null
    }

    let toBlocknumber = fromBlockNumber + config.blockProcessMax
    if (toBlocknumber > safeProcessBock) {
        toBlocknumber = safeProcessBock
    }

    return {
        from: fromBlockNumber,
        to: toBlocknumber
    }
}

async function doScan() {
    try {
        if (config == undefined || config == null) {
            config = await Model.ConfigModel.findOne({ key: 'ETH' })
            config = config.value
        }
        console.log(config)
        const processBlock = await _calculateProcessBlock()
        console.log(processBlock)
        if (!processBlock) return []

        console.log('************************************************************************')
        console.log("Start collecting from " + processBlock.from + " to " + processBlock.to + " at " + new Date().toLocaleString())

        const events = await BillboardContract.getPastEvents('SetMessage', {
            filter: {},
            fromBlock: processBlock.from,
            toBlock: processBlock.to
        })


        for (const event of events) {
            if (event.event === 'SetMessage') {
                const { blockNumber, transactionHash } = event
                const address = formatAddress(event.returnValues.user)
                const message = event.returnValues.message
                console.log('address:' + address)
                console.log("message:" + message)
                //check transaction exist{
                const transaction = await Model.MessageModel.findOne({ transactionHash: transactionHash })
                if (!transaction) {
                    await Model.MessageModel.create({
                        blockNumber: blockNumber,
                        transactionHash: transactionHash,
                        address: address,
                        message: message
                    })
                }
            }

        }
        config.blockProcessed = processBlock.to

        await Model.ConfigModel.update({ key: 'ETH' }, { value: config })
        // return transactions
    } catch (error) {
        console.log(error)
        return []
    }
}

const job = new CronJob('10,20,30,40,50 * * * * *', async function () {
    await doScan()
});
job.start();



