const mongoose = require('mongoose');

require('dotenv').config()

mongoose.Promise = global.Promise;
mongoose.set('useFindAndModify', false);

//// Connect DB
function connect() {
    console.log('string '+process.env[`${process.env.MODE}_DATABASE_STRING`])
    mongoose.connect(process.env[`${process.env.MODE}_DATABASE_STRING`], { useNewUrlParser: true, dbName: process.env[`${process.env.MODE}_DATABASE_NAME`]})
    mongoose.connection.on('error', error => console.log(error))
    mongoose.connection.once('open', () => console.log(`Connect to database successfully!!!`))
}

module.exports = {
    connect
}
