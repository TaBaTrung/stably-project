
require('dotenv').config()
const express = require('express')
const cors = require('cors')
require('./init')
const app = express();

app.use(cors())

//Routes
const messageRoute = require('./routes/message')
app.use('/api/message', messageRoute)

var port = process.env.PORT || '4000'
// start the server
app.listen(port, function () {
  console.log('Express server listening on port', port);
})
