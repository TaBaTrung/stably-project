'use strict'
const BaseModel = require('./base')
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const configSchema = new Schema({
    key: {
        type: String,
        required: true,
        index: true
    },
    value: {
        type: Object,
        required: true
    }
}, { timestamps: true })

const config = mongoose.model('config', configSchema)

class ConfigModel extends BaseModel {

}

module.exports = new ConfigModel(config)

