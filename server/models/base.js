class BaseModel {
    constructor(model) {
        this.model = model
    }

    async create(body, opts) {
        try {
            const data = await this.model.insertMany(body, opts)
            return data
        } catch (error) {
            console.log(error)
            return null
        }
    }

    async update(cond, body, opts={new: true }) {
        try {
            const data = await this.model.findOneAndUpdate(cond, { $set: body }, opts).exec()
            return data
        } catch (error) {
            console.log(error)
            return null
        }
    }

    async delete(cond) {
        try {
            const data = await this.model.deleteOne(cond).exec()
            return data
        } catch (error) {
            console.log(error)
            return null
        }
    }

    async deleteMany(cond) {
        try {
            const data = await this.model.deleteMany(cond).exec()
            return data
        } catch (error) {
            console.log(error)
            return null
        }
    }

    async findMany(cond, page = 1, limit = 20, sort = -1) {
        try {
            let skip = limit * (page - 1)
            const data = await this.model.find(cond).sort({createdAt: sort}).skip(skip).limit(limit).exec()
            if(data==null){
                return []
            }
            return data
        } catch (error) {
            console.log(`findMany error = ${error}`)
            return null
        }
    }

    async findManyWithSort(cond, page = 1, limit = 20, sort = { createdAt: -1 }) {
        try {
            let skip = limit * (page - 1)
            const data = await this.model.find(cond).sort(sort).skip(skip).limit(limit).exec()
            return data
        } catch (error) {
            console.log(error)
            return null
        }
    }

    async listDataTables(cond, start, length) {
        try {
            const data = await this.model.find(cond).sort({ createdAt: -1 }).skip(start).limit(length).exec()
            return data
        } catch (error) {
            console.log(error)
            return null
        }
    }

    async findOne(cond) {
        try {
            const data = await this.model.findOne(cond).exec()
            return data
        } catch (error) {
            console.log(error)
            return null
        }
    }

    async findOneWithSort(cond, sort = {updatedAt: -1}) {
        try {
            const data = await this.model.findOne(cond).sort(sort).exec()
            return data
        } catch (error) {
            console.log(error)
            return null
        }
    }

    async all(page = 1, limit = 20) {
        try {
            let skip = limit * (page - 1)
            const data = this.model.find({})
                .sort({ createdAt: -1 }) // 1 = asc , -1 = desc
                .skip(skip)
                .limit(limit)
            return data
        } catch (error) {
            console.log(error)
            return null
        }
    }

    async total(cond = {}) {
        return await this.model.countDocuments(cond);
    }
}

module.exports = BaseModel