'use strict'
const BaseModel = require('./base')
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const messageSchema = new Schema({
    address: {
        type: String,
        required: true,
        index: true
    },
    message: {
        type: String,
        required: true,
    },
    transactionHash: {
        type: String,
        required: true,
    },
    blockNumber: {
        type: Number,
        required: true
    }
}, { timestamps: true })

const message = mongoose.model('message', messageSchema)

class MesssageModel extends BaseModel {

}

module.exports = new MesssageModel(message)

