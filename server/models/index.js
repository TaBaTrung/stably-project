//connect database
const mongo = require('../config/database/mongo')
mongo.connect()

const MessageModel=require('./message')
const ConfigModel=require('./config')

module.exports={
    MessageModel,
    ConfigModel
}