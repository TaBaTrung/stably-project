const web3 = require('../config/blockchain/web3')
const Tx = require("ethereumjs-tx").Transaction

function formatAddress(address) {
    return  web3.utils.toChecksumAddress(address)
}

async function checkAddressValid(address) {
    return await web3.utils.isAddress(address);
}

async function getLatestBlockNumber(){
    return await web3.eth.getBlockNumber()
}

module.exports = {
    formatAddress, checkAddressValid, getLatestBlockNumber
}