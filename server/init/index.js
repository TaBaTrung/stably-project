const Model = require('../models')

async function init() {
    let ethConfig = await Model.ConfigModel.findOne({ key: 'ETH' })
    if (ethConfig == null) {
        await Model.ConfigModel.create({
            key: 'ETH',
            value: {
                confirmed: 1,
                blockProcessed: 9137451,
                blockProcessMax: 50
            }
        })
    }
}

init()