
const HttpStatus = require('http-status-codes')

//Model
const Model = require('../models')
const message = require('../models/message')

const {formatAddress}=require('../utils/eth')
class MessageController {
    async getMessages(req, res) {
        try {
            const address = formatAddress(req.params.address)
            let messages = await Model.MessageModel.findMany({ address: address })

            res.status(HttpStatus.OK).send(messages);
        } catch (error) {
            console.error(error);
            return res.status(HttpStatus.SERVICE_UNAVAILABLE).send({ error: error });
        }
    }
}

module.exports = new MessageController()