
if (window.ethereum) {
    window.web3 = new Web3(window.ethereum)
    try {
        // ask user for permission
        ethereum.enable()
        // user approved permission
    } catch (error) {
        // user rejected permission
        console.log('user rejected permission')
    }
}
else if (window.web3) {
    window.web3 = new Web3(window.web3.currentProvider)
    // no need to ask for permission
}
else {
    window.alert('Non-Ethereum browser detected. You should consider trying MetaMask!')
}
console.log(window.web3.currentProvider)

// contractAddress and abi are setted after contract deploy
var contractAddress = '0xB46B87ce01B18dC9F26fcd64d6834D79Ee631b16';
var abi = [
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": true,
                "internalType": "address",
                "name": "user",
                "type": "address"
            },
            {
                "indexed": false,
                "internalType": "string",
                "name": "message",
                "type": "string"
            }
        ],
        "name": "SetMessage",
        "type": "event"
    },
    {
        "inputs": [],
        "name": "adminWithdraw",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [],
        "name": "fee",
        "outputs": [
            {
                "internalType": "uint64",
                "name": "",
                "type": "uint64"
            }
        ],
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [],
        "name": "getMessage",
        "outputs": [
            {
                "internalType": "string",
                "name": "",
                "type": "string"
            }
        ],
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [],
        "name": "initialize",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "string",
                "name": "message",
                "type": "string"
            }
        ],
        "name": "setMessage",
        "outputs": [],
        "stateMutability": "payable",
        "type": "function"
    }
];

//contract instance
const contract = new web3.eth.Contract(abi, contractAddress);

// Accounts
var account;
const ethValue = web3.utils.toWei("0.25", "ether");

web3.eth.getAccounts(async function (err, accounts) {
    if (err != null) {
        alert("Error retrieving accounts.");
        return;
    }
    if (accounts.length == 0) {
        alert("No account found! Make sure the Ethereum client is configured properly.");
        return;
    }
    account = accounts[0];
    console.log('Account: ' + account);
    web3.eth.defaultAccount = account;
    if (account != undefined && account) {
        await initMessage(account)
    }

});

//Smart contract functions
function setMessage() {
    const info = $("#newInfo").val();
    contract.methods.setMessage(info).send({ from: account, value: ethValue }).then(function (tx) {
        console.log("Transaction: ", tx);
        initMessage()
    });
    $("#newInfo").val('');
}

function getMessage() {
    contract.methods.getMessage().call({ from: account }).then(function (info) {
        console.log("info: ", info);
        document.getElementById('lastInfo').innerHTML = info;
    });
}

// const endpoint = `http://54.254.162.235:4000/api/message/`
const endpoint = `http://localhost:4000/api/message/`
async function initMessage(account) {

    //url 
    const url = endpoint + account

    // const url = 'http://54.254.162.235:4000/api/message/0x34BD45D0746878d279a04036a1e2055cBBA1524A'
    console.log("load init", account);
    try {
        const response = await fetch(url)
        const text = await response.text();
        if (response.status == 200) {

            const data = JSON.parse(text)

            let nodeInfo = ``
            data.forEach(item => {
                nodeInfo += `<tr><td>${item["blockNumber"]}</td><td>${item["transactionHash"]}</td><td>${item["address"]}</td><td>${item["message"]}</td></tr>`
            });

            const tbody = document.getElementById('mess-info')
            if (tbody) {
                tbody.innerHTML = nodeInfo
            }
        }

        //add html to table

    } catch (error) {
        console.log(error.toString());
    }
}


// async function handleResponse(response) {
//     try {

//         const data = text && JSON.parse(text);
//         if (!response.ok) {
//             const error = (data && data.message) || response.statusText;
//             return Promise.reject(error);
//         }
//         return data;
//     } catch (error) {
//         _clearLocalStorage();
//     }
// }
