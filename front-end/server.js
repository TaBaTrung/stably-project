const express = require('express');
const cors = require('cors')
const app = express();
app.use(cors())
app.use(express.static(__dirname));
app.listen('3300');
console.log('Running at\nhttp://localhost:3300');