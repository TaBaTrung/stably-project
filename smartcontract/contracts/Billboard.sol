pragma solidity >=0.4.24 <0.7.0;
import "@openzeppelin/upgrades/contracts/Initializable.sol";

contract Billboard is Initializable{
	address private admin;
	uint64  public fee;
	mapping (address => string) private messages;

	event SetMessage(address indexed user, string message);

	modifier onlyAdmin {
        require(msg.sender == admin,"Only admin");
        _;
    }

	function initialize() public initializer {
        admin=msg.sender;
		fee=25;
    }

	function setMessage(string memory message) public payable{
		//check fee
		require(msg.value/10000000000000000 == fee , "Amount is invalid");
		messages[msg.sender]=message;
		emit SetMessage(msg.sender, message);
	}
	
	function getMessage() public view returns (string memory){
	    return messages[msg.sender];
	}
	
	function adminWithdraw() public onlyAdmin {
	    msg.sender.transfer(address(this).balance);
	}
}