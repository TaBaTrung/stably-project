const billboard = artifacts.require("./Billboard.sol")
const Web3 = require('web3')
const web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'))

contract('Billboard Contract', function (accounts) {
    before("setup ", async function () {

    });

    beforeEach(async function () {

    });
    const owner = accounts[0];
    const user1 = accounts[1];
    const user2 = accounts[2];
    const user3 = accounts[3];
    const user4 = accounts[4];

    const ethValue = web3.utils.toWei("0.25", "ether");

    console.log("owner: " + owner);

    it("test billboard contract ", async () => {
        console.log('****************************************************')
        const billboardContract = await billboard.new({ from: owner })
        console.log("\t billboard contract address " + billboardContract.address)

        console.log('\n\t billboard initialize')
        await billboardContract.initialize({ from: owner })

        console.log("\n\t user1 set message")
        const user1Message="message 1"
        await billboardContract.setMessage(user1Message,{ from: user1, value: ethValue })
        let user1MessageFromContract=await billboardContract.getMessage({from: user1})
        assert.equal(user1Message, user1MessageFromContract, "user 1 message incorrect")

        console.log("\n\t user2 set message")
        const user2Message="message 2"
        await billboardContract.setMessage(user2Message,{ from: user2, value: ethValue })
        let user2MessageFromContract=await billboardContract.getMessage({from: user2})
        assert.equal(user2Message, user2MessageFromContract, "user 2 message incorrect")

        console.log("\n\t user1 update message")
        const user1MessageUpdate="message 1 update"
        await billboardContract.setMessage(user1MessageUpdate,{ from: user1, value: ethValue })
        let user1MessageUpdateFromContract=await billboardContract.getMessage({from: user1})
        assert.equal(user1MessageUpdate, user1MessageUpdateFromContract, "user 1 message update incorrect")

        console.log("\n\t admin withdraw")
        const billboardBalance = await web3.eth.getBalance(billboardContract.address)
        assert.equal(billboardBalance, ethValue*3, "contract balance incorrect")
        const ownerBalanceBefore=await web3.eth.getBalance(owner)
        await billboardContract.adminWithdraw({ from: owner })
        const ownerBalanceAfter=await web3.eth.getBalance(owner)
        assert.equal(ownerBalanceAfter, parseInt(ownerBalanceBefore)+parseInt(billboardBalance), "owner balance incorrect")
    }).timeout(400000000);
});